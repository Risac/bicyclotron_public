package com.risac.bicyclotron;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EntityScan(basePackages = { "com.risac.bicyclotron" })
public class BicyclotronPublicApplication {

	public static void main(String[] args) {
		SpringApplication.run(BicyclotronPublicApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(BicyclotronApp app) {
		return (args) -> {
			app.go();
		};
	}

}
