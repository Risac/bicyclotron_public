package com.risac.bicyclotron.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.risac.bicyclotron.entities.CyclingData;
import com.risac.bicyclotron.repositories.CyclingDataRepository;

@Controller
public class MainController {

	@Autowired
	CyclingDataRepository repo;

	private static final Logger log = LogManager.getLogger(MainController.class.getName());

	@RequestMapping("/")

	public ModelAndView index(@ModelAttribute("data") CyclingData data) {
		ModelAndView modelAndView = new ModelAndView();

		List<CyclingData> findAll = repo.findAll();
		List<Boolean> isMoozh = new ArrayList<>();

		double maxDistance = findAll.stream().max(Comparator.comparing(CyclingData::getDistance))
				.orElseThrow(NoSuchElementException::new).getDistance();

		for (CyclingData cyclingData : findAll) {

			if (cyclingData.getDriver().toString().equals("MOOZH")) {
				isMoozh.add(true);
			} else {
				isMoozh.add(false);
			}
		}

		// TODO
		// Use Validator before saving

		boolean match = Stream
				.of(data.getDifficulty(), data.getDistance(), data.getMinutes(), data.getDriver(), data.getDate())
				.anyMatch(Objects::isNull);

		if (match == false) {
			log.info("Saving");
			repo.save(data);

			return new ModelAndView("redirect:/");
		}
		modelAndView.addObject("isMoozh", isMoozh);
		modelAndView.addObject("maxDistance", maxDistance);
		modelAndView.addObject("findAll", findAll);
		modelAndView.setViewName("index.html");
		return modelAndView;
	}

}
