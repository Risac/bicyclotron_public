package com.risac.bicyclotron.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface PersistedEntityRepository<T> extends CrudRepository<T, Long> {

	List<T> findAll();

}
