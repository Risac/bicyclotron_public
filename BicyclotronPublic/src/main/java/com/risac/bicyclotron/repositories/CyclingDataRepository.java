package com.risac.bicyclotron.repositories;

import com.risac.bicyclotron.entities.CyclingData;

public interface CyclingDataRepository extends PersistedEntityRepository<CyclingData> {

}
