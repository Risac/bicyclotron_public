package com.risac.bicyclotron;

import org.springframework.stereotype.Component;

@Component
public class BicyclotronApp {

	public void go() {
		System.err.println("'Ere we go!");
	}

}
