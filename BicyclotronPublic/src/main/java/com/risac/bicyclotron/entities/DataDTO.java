package com.risac.bicyclotron.entities;

import java.time.LocalDate;
import java.util.Objects;

public class DataDTO {

	private int difficulty;
	private double minutes;
	private double distance;

	private Driver driver;
	private LocalDate date;

	public DataDTO(int difficulty, double minutes, double distance, Driver driver, LocalDate date) {
		super();
		this.difficulty = difficulty;
		this.minutes = minutes;
		this.distance = distance;
		this.driver = driver;
		this.date = date;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public double getMinutes() {
		return minutes;
	}

	public void setMinutes(double minutes) {
		this.minutes = minutes;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, difficulty, distance, driver, minutes);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DataDTO other = (DataDTO) obj;
		return Objects.equals(date, other.date) && difficulty == other.difficulty
				&& Double.doubleToLongBits(distance) == Double.doubleToLongBits(other.distance)
				&& driver == other.driver && Double.doubleToLongBits(minutes) == Double.doubleToLongBits(other.minutes);
	}

	@Override
	public String toString() {
		return "DataDTO [difficulty=" + difficulty + ", minutes=" + minutes + ", distance=" + distance + ", driver="
				+ driver + ", date=" + date + "]";
	}

}
