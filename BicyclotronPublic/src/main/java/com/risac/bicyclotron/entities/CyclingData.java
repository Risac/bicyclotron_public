package com.risac.bicyclotron.entities;

import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity

public class CyclingData extends PersistedEntity {

	@NotNull(groups = { OnSave.class })
	private int difficulty;
	@NotNull
	private double minutes;
	@NotNull
	private double distance;
	@NotNull
	private Driver driver;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull
	private LocalDate date;

	public CyclingData() {
	}

	public CyclingData(int difficulty, double minutes, double distance, Driver driver, LocalDate date) {
		super();
		this.difficulty = difficulty;
		this.minutes = minutes;
		this.distance = distance;
		this.driver = driver;
		this.date = date;
	}

	public double getAverageSpeed() {
		double result = 60 / minutes * distance;
		result = Math.round(result);

		return result;

	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public double getMinutes() {
		return minutes;
	}

	public void setMinutes(double minutes) {
		this.minutes = minutes;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(date, difficulty, distance, driver, minutes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CyclingData other = (CyclingData) obj;
		return Objects.equals(date, other.date) && difficulty == other.difficulty
				&& Double.doubleToLongBits(distance) == Double.doubleToLongBits(other.distance)
				&& driver == other.driver && Double.doubleToLongBits(minutes) == Double.doubleToLongBits(other.minutes);
	}

	@Override
	public String toString() {
		return "CyclingData [difficulty=" + difficulty + ", minutes=" + minutes + ", distance=" + distance + ", driver="
				+ driver + ", date=" + date + "]";
	}

	class OnSave {
	}

}
